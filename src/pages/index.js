import React, { useState, useRef } from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './styles.module.css';

const Stopwatch = () => {
  const [timer, setTimer] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const [isPaused, setIsPaused] = useState(false);
  const increment = useRef(null);

  const handleStart = () => {
    setIsActive(true);
    setIsPaused(true);
    increment.current = setInterval(() => {
      setTimer((t) => t + 1);
    }, 10);
  };

  const handleReset = () => {
    clearInterval(increment.current);
    setIsActive(false);
    setIsPaused(false);
    setTimer(0);
  };

  const handlePause = () => {
    clearInterval(increment.current);
    setIsPaused(false);
  };

  const handleResume = () => {
    setIsPaused(true);
    increment.current = setInterval(() => {
      setTimer((t) => t + 1);
    }, 10);
  };

  const formatTime = () => {
    const getCentiSeconds = `0${(timer % 100)}`.slice(-2);
    const seconds = `${Math.floor(timer / 100)}`;
    const getSeconds = `0${(seconds % 60)}`.slice(-2);
    const minutes = `${Math.floor(timer / 6000)}`;
    const getMinutes = `0${minutes % 60}`.slice(-2);
    const getHours = `0${Math.floor(timer / 360000)}`.slice(-2);

    return `${getHours} : ${getMinutes} : ${getSeconds} : ${getCentiSeconds}`;
  };

  return (
    <div className="stopWatch">
      <div>
        <p className="timer">{formatTime()}</p>
        <div>
          {
            !isActive && !isPaused ? (
              <button type="button" className="start" onClick={handleStart}>
                Start
              </button>
            )
              : (
                isPaused
                  ? <button type="button" className="pause" onClick={handlePause}>Pause</button>
                  : <button type="button" className="cont" onClick={handleResume}>Continue</button>
              )
          }
          <button type="button" className="reset" onClick={handleReset} disabled={!isActive}>
            Reset
          </button>
        </div>
      </div>
    </div>
  );
};

export default function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />"
    >
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
        </div>
      </header>
      <main>
        <Stopwatch />
      </main>
    </Layout>
  );
}
