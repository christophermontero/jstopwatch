/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Welcome to Jstopwatch',
  tagline: 'A stopwatch powered by Docusaurus',
  url: 'https://christophermontero.gitlab.io',
  baseUrl: '/jstopwatch/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'christophermontero', // Usually your GitHub org/user name.
  projectName: 'jstopwatch', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Jstopwatch',
      logo: {
        alt: 'Docusaurus',
        src: 'img/logo.svg'
      }
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              href: 'https://docusaurus.io/docs/installation'
            }
          ]
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Stack Overflow',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus'
            },
            {
              label: 'Discord',
              href: 'https://discordapp.com/invite/docusaurus'
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/docusaurus'
            }
          ]
        },
        {
          title: 'More',
          items: [
            {
              label: 'GitHub',
              href: 'https://github.com/facebook/docusaurus'
            }
          ]
        }
      ],
      copyright: `Copyright © ${new Date().getFullYear()} JStopwatch, Inc. Built with Docusaurus.`
    }
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/christophermontero/jstopwatch.git'
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/christophermontero/jstopwatch.git'
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css')
        }
      }
    ]
  ]
};
